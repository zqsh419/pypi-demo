.. JamPypi documentation master file, created by
   sphinx-quickstart on Sun Sep 19 00:37:11 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. mdinclude:: gitlab.md
.. mdinclude:: sphinx.md

JamPypi API
===================================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   jam_pypi

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
