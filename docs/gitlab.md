# A testing repo for publishing pypi via CI

## gitlab

- set variable `GITLAB_PYPI_NAME`, `GITLAB_PYPI_PWD` and `PYPI_TOKEN` in settings/ci


## reference

- [Help discussion on poetry ci](https://github.com/python-poetry/poetry/discussions/4205)
