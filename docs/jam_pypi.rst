jam\_pypi package
=================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   jam_pypi.helper

Submodules
----------

jam\_pypi.addition module
-------------------------

.. automodule:: jam_pypi.addition
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: jam_pypi
   :members:
   :undoc-members:
   :show-inheritance:
