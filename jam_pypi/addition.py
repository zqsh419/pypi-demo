import numpy as np

Array = np.array


def add(x: Array, y: Array) -> Array:
    """add two Array

    :param x: comments for x
    :type x: Array
    :param y: comments for y
    :type y: Array
    :return: summation results
    :rtype: Array
    """
    return x + y
